package cn.tedu.dbinit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootApplication
public class DbInitApplication {
    @Autowired
    private DataSource dataSource;

    public static void main(String[] args) {
        SpringApplication.run(DbInitApplication.class, args);
    }

    // 创建完所有实例并执行完所有依赖注入之后执行
    @PostConstruct
    public void init() throws SQLException {
        //执行四个 sql 脚本文件
        exec("sql/account.sql");
        exec("sql/order.sql");
        exec("sql/seata-server.sql");
        exec("sql/storage.sql");
    }

    private void exec(String file) throws SQLException {

        ClassPathResource cpr =
                new ClassPathResource(file, DbInitApplication.class.getClassLoader());
        EncodedResource resource = new EncodedResource(cpr);

        ScriptUtils.executeSqlScript(
                dataSource.getConnection(),
                resource);
    }
}
