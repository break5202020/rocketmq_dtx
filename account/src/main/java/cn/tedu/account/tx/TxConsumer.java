package cn.tedu.account.tx;

import cn.tedu.account.entity.AccountMessage;
import cn.tedu.account.service.AccountService;
import cn.tedu.account.util.JsonUtil;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;

//消费者自动创建实例时，自动启动
@RocketMQMessageListener(consumerGroup = "account-consumer-group",
        topic = "order-topic", selectorExpression = "account")
public class TxConsumer implements RocketMQListener {

    @Autowired
    private AccountService accountService;

    @Override
    public void onMessage(Object json) {
        AccountMessage am = JsonUtil.from(json, AccountMessage.class);

        accountService.decrease(am.getUserId(),am.getMoney());
    }
}
