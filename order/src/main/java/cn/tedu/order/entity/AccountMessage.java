package cn.tedu.order.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
//封装发送给账户服务的数据：用户id和扣减金额。另外还封装了事务id。
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountMessage {
    private Long userId;
    private BigDecimal money;
    private String xid;
}
