package cn.tedu.order.mapper;

import cn.tedu.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface OrderMapper extends BaseMapper<Order> {
    // 从通用Mapper集成的 insert(Order)
    void create(Order order); // insert into ....
}
