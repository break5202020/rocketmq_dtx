package cn.tedu.order.service;

import cn.tedu.order.entity.Order;
import cn.tedu.order.entity.TxInfo;
import cn.tedu.order.mapper.TxMapper;
import cn.tedu.order.util.JsonUtil;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.transaction.annotation.Transactional;

import javax.transaction.xa.Xid;

//这个监听器，自动创建实例，自动注册监听器
@RocketMQTransactionListener
public class TxListener implements RocketMQLocalTransactionListener {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private TxMapper txMapper;

    //执行本地事务
    @Override
    @Transactional
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        //保存状态的变量
        RocketMQLocalTransactionState state;  //用来返回
        int status;  //保存到数据库

        try {
            Order order = (Order) o;
            orderService.doCreate(order);
            //事务执行成功
            state = RocketMQLocalTransactionState.COMMIT;
            status = 0;
        }catch (Exception e){
            //事务执行失败
            state = RocketMQLocalTransactionState.ROLLBACK;
            status = 1;
        }

        //保存事务状态
        byte[] payload = (byte[]) message.getPayload();
        String json = new String(payload);
        String xid = JsonUtil.getString(json, "xid");

        TxInfo txInfo = new TxInfo(xid,status,System.currentTimeMillis());
        txMapper.insert(txInfo);

        //返回事务执行状态
        return state;
    }

    //处理事务回查
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        byte[] payload = (byte[]) message.getPayload();
        String json = new String(payload);
        String xid = JsonUtil.getString(json, "xid");

        TxInfo txInfo = txMapper.selectById(xid); //从数据库查询事务状态
        if (txInfo == null){
            return RocketMQLocalTransactionState.UNKNOWN;
        }

        switch (txInfo.getStatus()){
            case 0: return RocketMQLocalTransactionState.COMMIT;
            case 1: return RocketMQLocalTransactionState.ROLLBACK;
            default: return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
