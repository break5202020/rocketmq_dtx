package cn.tedu.order.service;

import cn.tedu.order.entity.AccountMessage;
import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.util.JsonUtil;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.transaction.xa.Xid;
import java.util.Random;
import java.util.UUID;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private AccountClient accountClient;
//    @Autowired
//    private StorageClient storageClient;

    @Autowired
    private RocketMQTemplate rocketMQ;


    @Override
    public void create(Order order) {
        //在业务方法不执行业务处理，而是发送"事务消息"，触发监听器来调用业务代码

        //自定义事务id，把“-”去掉
        String xid = UUID.randomUUID().toString().replace("-", "");

        //创建消息数据的封装对象
        AccountMessage am = new AccountMessage(order.getUserId(), order.getMoney(), xid);

        //转成 json 字符串
        String json = JsonUtil.to(am);

        //json字符串封装到 Spring Message 对象
        Message<String> msg = MessageBuilder.withPayload(json).build();


        //如果有标记，在目标位置后用":"拼接，
        rocketMQ.sendMessageInTransaction("order-topic:account",msg,order);
    }


    public void doCreate(Order order) {
        // 远程调用“全局唯一id发号器”，获取orderId
        String s = easyIdClient.nextId("order_business");
        Long orderId = Long.valueOf(s);

        order.setId(orderId);
        orderMapper.create(order);

//        // 远程调用库存，减少商品库存
//        storageClient.decrease(order.getProductId(), order.getCount());
//        // 远程调用账户，扣减账户金额
//        accountClient.decrease(order.getUserId(), order.getMoney());
    }


}
